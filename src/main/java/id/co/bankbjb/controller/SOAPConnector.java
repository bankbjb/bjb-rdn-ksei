/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.bankbjb.controller;

import id.co.bankbjb.ConfigProperties;
import id.co.bankbjb.rdn.ksei.wsdl.CFileContent;
import id.co.bankbjb.rdn.ksei.wsdl.CheckConnection;
import id.co.bankbjb.rdn.ksei.wsdl.CheckConnectionResponse;
import id.co.bankbjb.rdn.ksei.wsdl.ObjectFactory;
import id.co.bankbjb.rdn.ksei.wsdl.ReceiveDataInvestor;
import id.co.bankbjb.rdn.ksei.wsdl.ReceiveDataInvestorResponse;
import id.co.bankbjb.rdn.ksei.wsdl.ReceiveDataStatic;
import id.co.bankbjb.rdn.ksei.wsdl.ReceiveDataStaticResponse;
import id.co.bankbjb.rdn.ksei.wsdl.ValidatorDataInvestor;
import id.co.bankbjb.rdn.ksei.wsdl.ValidatorDataInvestorResponse;
import id.co.bankbjb.rdn.logger.interceptor.LogHttpHeaderClientInterceptor;
import java.io.IOException;
import java.net.SocketTimeoutException;
import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;
import org.springframework.ws.soap.SoapHeader;
import org.springframework.ws.soap.SoapHeaderElement;
import org.springframework.ws.soap.client.core.SoapActionCallback;
import org.springframework.ws.soap.saaj.SaajSoapMessage;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;

/**
 *
 * @author LENOVO
 */
public class SOAPConnector extends WebServiceGatewaySupport {
    
    private static final Logger logger = LoggerFactory.getLogger(SOAPConnector.class);

    @Autowired
    private ConfigProperties properties;
    
    public Object callWebService(String url, Object request) {
        return getWebServiceTemplate().marshalSendAndReceive(url, request);
    }

    public ReceiveDataInvestorResponse receiveDataInvestor(String url, String memberCode, byte[] fileContent) throws SOAPException {
        String action = "http://tempuri.org/ReceiveDataInvestor";
//        String uri = "http://10.74.4.209:9001/OSB_AKSES/ProxyService/ProxyServiceOSB";
//        String uri = "http://10.112.6.14:9001/OSB_AKSES/ProxyService/ProxyServiceOSB";
        String uri = url;
        
        CFileContent cFileContent = new ObjectFactory().createCFileContent();
        cFileContent.setFileContents(fileContent);
        cFileContent.setSizeInBytes(1000);
        cFileContent.setCrc32(-1);
        
        JAXBElement<CFileContent> fileResponse = new ObjectFactory().createGetFileResponse(cFileContent);
        
        ReceiveDataInvestor request = new ObjectFactory().createReceiveDataInvestor();
        request.setMemberCode(memberCode);
        request.setGetFileResponse(fileResponse.getValue());
        
        logger.info("Request receive data investor with code: " + request.getMemberCode());
        
        MessageFactory messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
        SaajSoapMessageFactory saajSoapMessageFactory = new SaajSoapMessageFactory(messageFactory);
        
        // register the LogHttpHeaderClientInterceptor
        ClientInterceptor[] interceptors = new ClientInterceptor[] {new LogHttpHeaderClientInterceptor()};
        
        WebServiceTemplate webServiceTemplate = getWebServiceTemplate();
        webServiceTemplate.setMessageFactory(saajSoapMessageFactory);
        webServiceTemplate.setInterceptors(interceptors);
        
        SoapActionCallback requestCallback = new SoapActionCallback(action) {
            @Override
            public void doWithMessage(WebServiceMessage message) throws IOException {
                SaajSoapMessage soapMessage = (SaajSoapMessage) message;
                SoapHeader soapHeader = soapMessage.getSoapHeader();

                QName wsaToQName = new QName("http://www.w3.org/2005/08/addressing", "To", "wsa");
                SoapHeaderElement wsaTo = soapHeader.addHeaderElement(wsaToQName);
                wsaTo.setText(uri);

                QName wsaActionQName = new QName("http://www.w3.org/2005/08/addressing", "Action", "wsa");
                SoapHeaderElement wsaAction = soapHeader.addHeaderElement(wsaActionQName);
                wsaAction.setText(action);
            }
        };
        
        ReceiveDataInvestorResponse response = (ReceiveDataInvestorResponse) webServiceTemplate.marshalSendAndReceive(uri, request, requestCallback);
        
        return response;
    }
    
    public ReceiveDataStaticResponse receiveDataStatic(String url, String memberCode, byte[] fileContent) 
            throws SOAPException, SocketTimeoutException {
        String action = "http://tempuri.org/ReceiveDataStatic";
//        String uri = "http://10.74.4.209:9001/OSB_AKSES/ProxyService/ProxyServiceOSB";
//        String uri = "http://10.112.6.14:9001/OSB_AKSES/ProxyService/ProxyServiceOSB";
        String uri = url;
        
        CFileContent cFileContent = new ObjectFactory().createCFileContent();
        cFileContent.setFileContents(fileContent);
        cFileContent.setSizeInBytes(1000);
        cFileContent.setCrc32(-1);
        
        JAXBElement<CFileContent> fileResponse = new ObjectFactory().createGetFileResponse(cFileContent);
        
        ReceiveDataStatic request = new ObjectFactory().createReceiveDataStatic();
        request.setMemberCode(memberCode);
        request.setGetFileResponse(fileResponse.getValue());
        
        logger.info("Request receive data static with code: " + request.getMemberCode());
        
        MessageFactory messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
        SaajSoapMessageFactory saajSoapMessageFactory = new SaajSoapMessageFactory(messageFactory);
        
        // register the LogHttpHeaderClientInterceptor
        ClientInterceptor[] interceptors = new ClientInterceptor[] {new LogHttpHeaderClientInterceptor()};
        
        WebServiceTemplate webServiceTemplate = getWebServiceTemplate();
        webServiceTemplate.setMessageFactory(saajSoapMessageFactory);
        webServiceTemplate.setInterceptors(interceptors);
        
        SoapActionCallback requestCallback = new SoapActionCallback(action) {
            @Override
            public void doWithMessage(WebServiceMessage message) throws IOException {
                SaajSoapMessage soapMessage = (SaajSoapMessage) message;
                SoapHeader soapHeader = soapMessage.getSoapHeader();

                QName wsaToQName = new QName("http://www.w3.org/2005/08/addressing", "To", "wsa");
                SoapHeaderElement wsaTo = soapHeader.addHeaderElement(wsaToQName);
                wsaTo.setText(uri);

                QName wsaActionQName = new QName("http://www.w3.org/2005/08/addressing", "Action", "wsa");
                SoapHeaderElement wsaAction = soapHeader.addHeaderElement(wsaActionQName);
                wsaAction.setText(action);
            }
        };
        
        ReceiveDataStaticResponse response = (ReceiveDataStaticResponse) webServiceTemplate.marshalSendAndReceive(uri, request, requestCallback);
        
        return response;
    }
    
    public ValidatorDataInvestorResponse validatorDataInvestor(String url, String memberCode, byte[] fileContent) 
            throws SOAPException, SocketTimeoutException {
        String action = "http://tempuri.org/ValidatorDataInvestor";
//        String uri = "http://10.74.4.209:9001/OSB_AKSES/ProxyService/ProxyServiceOSB";
//        String uri = "http://10.112.6.14:9001/OSB_AKSES/ProxyService/ProxyServiceOSB";
        String uri = url;
        
        CFileContent cFileContent = new ObjectFactory().createCFileContent();
        cFileContent.setFileContents(fileContent);
        cFileContent.setSizeInBytes(1000);
        cFileContent.setCrc32(-1);
        
        JAXBElement<CFileContent> fileResponse = new ObjectFactory().createGetFileResponse(cFileContent);
        
        ValidatorDataInvestor request = new ObjectFactory().createValidatorDataInvestor();
        request.setMemberCode(memberCode);
        request.setGetFileResponse(fileResponse.getValue());
        
        logger.info("Request validator data investor with code: " + request.getMemberCode());
        
        MessageFactory messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
        SaajSoapMessageFactory saajSoapMessageFactory = new SaajSoapMessageFactory(messageFactory);
        
        // register the LogHttpHeaderClientInterceptor
        ClientInterceptor[] interceptors = new ClientInterceptor[] {new LogHttpHeaderClientInterceptor()};
        
        WebServiceTemplate webServiceTemplate = getWebServiceTemplate();
        webServiceTemplate.setMessageFactory(saajSoapMessageFactory);
        webServiceTemplate.setInterceptors(interceptors);
        
        SoapActionCallback requestCallback = new SoapActionCallback(action) {
            @Override
            public void doWithMessage(WebServiceMessage message) throws IOException {
                SaajSoapMessage soapMessage = (SaajSoapMessage) message;
                SoapHeader soapHeader = soapMessage.getSoapHeader();

                QName wsaToQName = new QName("http://www.w3.org/2005/08/addressing", "To", "wsa");
                SoapHeaderElement wsaTo = soapHeader.addHeaderElement(wsaToQName);
                wsaTo.setText(uri);

                QName wsaActionQName = new QName("http://www.w3.org/2005/08/addressing", "Action", "wsa");
                SoapHeaderElement wsaAction = soapHeader.addHeaderElement(wsaActionQName);
                wsaAction.setText(action);
            }
        };
        
        ValidatorDataInvestorResponse response = (ValidatorDataInvestorResponse) webServiceTemplate.marshalSendAndReceive(uri, request, requestCallback);
        
        return response;
    }
    
    public CheckConnectionResponse checkConnection(String url, CheckConnection request) throws SOAPException {
        String action = "http://tempuri.org/CheckConnection";
//        String uri = "http://10.74.4.209:9001/OSB_AKSES/ProxyService/ProxyServiceOSB";
//        String uri = "http://10.112.6.14:9001/OSB_AKSES/ProxyService/ProxyServiceOSB";
        String uri = url;

        logger.info("Request check connection with code: " + request.getMemberCode());

        MessageFactory messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
        SaajSoapMessageFactory saajSoapMessageFactory = new SaajSoapMessageFactory(messageFactory);
        
        // register the LogHttpHeaderClientInterceptor
        ClientInterceptor[] interceptors = new ClientInterceptor[] {new LogHttpHeaderClientInterceptor()};
        
        WebServiceTemplate webServiceTemplate = getWebServiceTemplate();
        webServiceTemplate.setMessageFactory(saajSoapMessageFactory);
        webServiceTemplate.setInterceptors(interceptors);

        SoapActionCallback requestCallback = new SoapActionCallback(action) {
            @Override
            public void doWithMessage(WebServiceMessage message) throws IOException {
                SaajSoapMessage soapMessage = (SaajSoapMessage) message;
                SoapHeader soapHeader = soapMessage.getSoapHeader();

                QName wsaToQName = new QName("http://www.w3.org/2005/08/addressing", "To", "wsa");
                SoapHeaderElement wsaTo = soapHeader.addHeaderElement(wsaToQName);
                wsaTo.setText(uri);

                QName wsaActionQName = new QName("http://www.w3.org/2005/08/addressing", "Action", "wsa");
                SoapHeaderElement wsaAction = soapHeader.addHeaderElement(wsaActionQName);
                wsaAction.setText(action);
            }
        };
        
        CheckConnectionResponse response = (CheckConnectionResponse) webServiceTemplate.marshalSendAndReceive(uri, request, requestCallback);
        
        return response;
    }
}
