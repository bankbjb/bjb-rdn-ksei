/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.bankbjb.controller;

import id.co.bankbjb.ConfigProperties;
import id.co.bankbjb.rdn.model.KseiRdnAccountHistory;
import id.co.bankbjb.rdn.ksei.wsdl.ReceiveDataStaticResponse;
import id.co.bankbjb.rdn.ksei.wsdl.ValidatorDataInvestor;
import id.co.bankbjb.rdn.ksei.wsdl.ValidatorDataInvestorResponse;
import id.co.bankbjb.rdn.model.KseiRdnAccount;
import id.co.bankbjb.rdn.model.KseiRdnAccountId;
import id.co.bankbjb.rdn.repository.KseiRdnAccountHistoryRepository;
import id.co.bankbjb.rdn.repository.KseiRdnAccountRepository;
import java.io.File;
import java.io.StringWriter;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author LENOVO
 */
@RestController
public class RdnIncomingController {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(RdnIncomingController.class);

//    private static final String bankCode = "BJB03";
//    private static final String rdnKseiUri = "http://10.112.6.14:9001/OSB_AKSES/ProxyService/ProxyServiceOSB";
    @Autowired
    private ConfigProperties properties;
    @Autowired
    private KseiRdnAccountRepository accountRepository;
    @Autowired
    private KseiRdnAccountHistoryRepository accountHistoryRepository;

    @PostMapping(path = "/rdn/report-data-static", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public String reportDataStatic(@RequestBody String requestBody) {
        JSONObject response = new JSONObject(requestBody);
        JSONObject responseRC = new JSONObject();

//        String[] returnContents = buildContentRequestReportDataStatic(response, bankCode);
        Object[] returnContents = buildContentRequestReportDataStatic(response, properties.getBankCode());
        String fileContent = (String) returnContents[1];
        logger.info("REQUEST CONTENTS: " + fileContent);

//        JSONArray data = response.getJSONArray("data");
        JSONArray data = (JSONArray) returnContents[0];
//        JSONObject data0 = data.getJSONObject(0);
//        data0.put("extReff", returnContents[0]);
//        data.put(0, data0);
        response.put("data", data);
        response.put("batchReff", (String) returnContents[2]);

        for (int i = 0; i < data.length(); i++) {
            JSONObject accountData = data.getJSONObject(i);

            Optional<KseiRdnAccount> kseiRdnAccountOpt = accountRepository.findById(new KseiRdnAccountId(accountData.getString("sidNumber"), accountData.getString("accountNumber")));

            KseiRdnAccount kseiRdnAccount = kseiRdnAccountOpt.get();
            kseiRdnAccount.setSid(accountData.getString("sidNumber"));
            kseiRdnAccount.setSre(accountData.getString("accountNumber"));
            kseiRdnAccount.setRdn(accountData.getString("bankAccountNumber"));
            kseiRdnAccount.setDatastaticBatchReff((String) returnContents[2]);
            kseiRdnAccount.setDatastaticReff(accountData.getString("extReff"));

            accountRepository.save(kseiRdnAccount);
            
            // save to history
            KseiRdnAccountHistory accountHistory = new KseiRdnAccountHistory();
            accountHistory.setReffId(accountData.getString("extReff"));
            accountHistory.setPeId(accountData.getString("accountNumber").substring(0, 5));
            accountHistory.setSid(accountData.getString("sidNumber"));
            accountHistory.setSre(accountData.getString("accountNumber"));
            accountHistory.setRdn(accountData.getString("bankAccountNumber"));
            accountHistory.setName(accountData.getString("investorName"));
            accountHistory.setActivity(accountData.getString("activity"));
            accountHistory.setActivityDate(new Date());
            
            accountHistoryRepository.save(accountHistory);
        }

        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        // this is the package name specified in the <generatePackage> specified in
        // pom.xml
        marshaller.setContextPath("id.co.bankbjb.rdn.ksei.wsdl");

        SOAPConnector soapConnector = new SOAPConnector();
//        soapConnector.setDefaultUri(rdnKseiUri);
        soapConnector.setDefaultUri(properties.getUrl());
        soapConnector.setMarshaller(marshaller);
        soapConnector.setUnmarshaller(marshaller);

        ReceiveDataStaticResponse soapResponse = null;
        try {
//            soapResponse = soapConnector.receiveDataStatic(bankCode, fileContent.getBytes());
            soapResponse = soapConnector.receiveDataStatic(properties.getUrl(), properties.getBankCode(), fileContent.getBytes());

            System.err.println("result: " + soapResponse.getReceiveDataStaticResult());

            switch (soapResponse.getReceiveDataStaticResult()) {
                case 1:
                    responseRC.put("RC", "RDN-00");
                    responseRC.put("RCMSG", "SUCCESS");
                    break;
                case 0:
                    responseRC.put("RC", "RDNKSEI-01");
                    responseRC.put("RCMSG", "ERROR: Request failed to read by remote host");
                    break;
                default:
                    responseRC.put("RC", "RDN-99");
                    responseRC.put("RCMSG", "ERROR: Other");
                    break;
            }
        } catch (SOAPException ex) {
            logger.error("ERROR_UNDEFINED: ", ex);

            responseRC.put("RC", "RDN-98");
            responseRC.put("RCMSG", "ERROR_UNDEFINED: " + ex.getMessage());
        } catch (SocketTimeoutException ex) {
            logger.error("ERROR: " + ex.getMessage());

            responseRC.put("RC", "RDN-91");
            responseRC.put("RCMSG", "ERROR_CONNECTION");
        } catch (Exception ex) {
            logger.error("ERROR_UNKNOWN: ", ex);

            responseRC.put("RC", "RDN-99");
            responseRC.put("RCMSG", "ERROR_UNKNOWN" + ex.getMessage());
        }

        response.put("RC", responseRC);

        return response.toString();
    }

    @PostMapping(path = "/rdn/data-validation", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public String dataValidation(@RequestBody String requestBody) {
        JSONObject response = new JSONObject(requestBody);
        JSONObject responseRC = new JSONObject();

        Object[] returnContents = buildContentRequestDataValidation(response);
//        String fileContent = buildContentRequestDataValidation(response, bankCode);
        String fileContent = (String) returnContents[1];
        logger.info("REQUEST CONTENTS: " + fileContent);

//        JSONArray data = response.getJSONArray("data");
        JSONArray data = (JSONArray) returnContents[0];
//        JSONObject data0 = data.getJSONObject(0);
//        data0.put("extReff", returnContents[0]);
//        data.put(0, data0);
        response.put("data", data);

        for (int i = 0; i < data.length(); i++) {
            JSONObject accountData = data.getJSONObject(i);

            KseiRdnAccount kseiRdnAccount = new KseiRdnAccount();
            kseiRdnAccount.setSid(accountData.getString("sidNumber"));
            kseiRdnAccount.setSre(accountData.getString("accountNumber"));
            kseiRdnAccount.setValidationReff(accountData.getString("extReff"));

            accountRepository.save(kseiRdnAccount);
        }

        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        // this is the package name specified in the <generatePackage> specified in
        // pom.xml
        marshaller.setContextPath("id.co.bankbjb.rdn.ksei.wsdl");

        SOAPConnector soapConnector = new SOAPConnector();
//        soapConnector.setDefaultUri(rdnKseiUri);
        soapConnector.setDefaultUri(properties.getUrl());
        soapConnector.setMarshaller(marshaller);
        soapConnector.setUnmarshaller(marshaller);

        ValidatorDataInvestorResponse soapResponse = null;
        try {
//            soapResponse = soapConnector.validatorDataInvestor(bankCode, fileContent.getBytes());
            soapResponse = soapConnector.validatorDataInvestor(properties.getUrl(), properties.getBankCode(), fileContent.getBytes());

            System.err.println("result: " + soapResponse.getValidatorDataInvestorResult());

            switch (soapResponse.getValidatorDataInvestorResult()) {
                case 1:
                    responseRC.put("RC", "RDN-00");
                    responseRC.put("RCMSG", "SUCCESS");
                    break;
                case 0:
                    responseRC.put("RC", "RDNKSEI-01");
                    responseRC.put("RCMSG", "ERROR: Request failed to read by remote host");
                    break;
                default:
                    responseRC.put("RC", "RDN-99");
                    responseRC.put("RCMSG", "ERROR: Other");
                    break;
            }
        } catch (SOAPException ex) {
            logger.error("ERROR_UNDEFINED: ", ex);

            responseRC.put("RC", "RDN-98");
            responseRC.put("RCMSG", "ERROR_UNDEFINED: " + ex.getMessage());
        } catch (SocketTimeoutException ex) {
            logger.error("ERROR: " + ex.getMessage());

            responseRC.put("RC", "RDN-91");
            responseRC.put("RCMSG", "ERROR_CONNECTION");
        } catch (Exception ex) {
            logger.error("ERROR_UNKNOWN: ", ex);

            responseRC.put("RC", "RDN-99");
            responseRC.put("RCMSG", "ERROR_UNKNOWN: " + ex.getMessage());
        }

        response.put("RC", responseRC);

        return response.toString();
    }

    @PostMapping(path = "/rdn/account-statement", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public String investorAccountStatement(@RequestBody String requestBody) {
        JSONObject response = new JSONObject(requestBody);

        JSONObject responseRC = new JSONObject();
        responseRC.put("RC", "RDN-99");
        responseRC.put("RCMSG", "ERROR: Not yet implemented");

        response.put("RC", responseRC);

        return response.toString();
    }

    private Object[] buildContentRequestReportDataStatic(JSONObject jsonRequest, String bankCode) {
        Object[] returnArray = new Object[3];
        JSONArray arrDataRes = new JSONArray();

        Date currentDate = new Date();
        String stringResult = null;

        String batchReff = "bjbbatch" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(currentDate);

        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("Message");
            rootElement.setAttribute("name", "dataStaticInvestor");
            rootElement.setAttribute("type", "IncomingMessage");

            doc.appendChild(rootElement);

            if (jsonRequest.has("batchReff") && !jsonRequest.getString("batchReff").isEmpty()) {
                batchReff = jsonRequest.getString("batchReff");
            }

            // Field elements
            Element fieldBatchReference = doc.createElement("Field");
            fieldBatchReference.setAttribute("name", "batchReference");
            fieldBatchReference.appendChild(doc.createTextNode(batchReff));

            rootElement.appendChild(fieldBatchReference);

            // List elements
            Element list = doc.createElement("List");
            list.setAttribute("name", "data");

            rootElement.appendChild(list);

            //loop data here
            JSONArray arrData = jsonRequest.getJSONArray("data");

            for (int i = 0; i < arrData.length(); i++) {
                JSONObject objData = arrData.getJSONObject(i);

                // Record elements
                Element record = doc.createElement("Record");
                record.setAttribute("name", "data");

                list.appendChild(record);

                String extReff = "bjb" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());

                //get "externalReference" from request data if exist
                if (jsonRequest.getJSONArray("data").getJSONObject(i).has("extReff")
                        && !jsonRequest.getJSONArray("data").getJSONObject(i).getString("extReff").isEmpty()) {
                    extReff = jsonRequest.getJSONArray("data").getJSONObject(i).getString("extReff");
                }
//                returnArray[0] = extReff;
                objData.put("extReff", extReff);
                arrDataRes.put(objData);

                Element fieldExternalReference = doc.createElement("Field");
                fieldExternalReference.setAttribute("name", "externalReference");
                fieldExternalReference.appendChild(doc.createTextNode(extReff));
                record.appendChild(fieldExternalReference);

                if (objData.has("accountNumber") && objData.getString("accountNumber").length() > 4) {
                    Element fieldParticipantID = doc.createElement("Field");
                    fieldParticipantID.setAttribute("name", "participantID");
                    fieldParticipantID.appendChild(doc.createTextNode(objData.getString("accountNumber").substring(0, 5)));
                    record.appendChild(fieldParticipantID);

                    Element fieldParticipantName = doc.createElement("Field");
                    fieldParticipantName.setAttribute("name", "participantName");
                    fieldParticipantName.appendChild(doc.createTextNode(objData.getString("accountNumber").substring(0, 5)));
                    record.appendChild(fieldParticipantName);

                    if (objData.getString("accountNumber").length() < 14) {
                        Element fieldAccountNumber = doc.createElement("Field");
                        fieldAccountNumber.setAttribute("name", "accountNumber");
                        fieldAccountNumber.appendChild(doc.createTextNode(""));
                        record.appendChild(fieldAccountNumber);
                    } else {
                        Element fieldAccountNumber = doc.createElement("Field");
                        fieldAccountNumber.setAttribute("name", "accountNumber");
                        fieldAccountNumber.appendChild(doc.createTextNode(objData.getString("accountNumber")));
                        record.appendChild(fieldAccountNumber);
                    }
                } else {
                    Element fieldParticipantID = doc.createElement("Field");
                    fieldParticipantID.setAttribute("name", "participantID");
                    fieldParticipantID.appendChild(doc.createTextNode(""));
                    record.appendChild(fieldParticipantID);

                    Element fieldParticipantName = doc.createElement("Field");
                    fieldParticipantName.setAttribute("name", "participantName");
                    fieldParticipantName.appendChild(doc.createTextNode(""));
                    record.appendChild(fieldParticipantName);

                    Element fieldAccountNumber = doc.createElement("Field");
                    fieldAccountNumber.setAttribute("name", "accountNumber");
                    fieldAccountNumber.appendChild(doc.createTextNode(""));
                    record.appendChild(fieldAccountNumber);
                }

                Element fieldInvestorName = doc.createElement("Field");
                fieldInvestorName.setAttribute("name", "investorName");
                fieldInvestorName.appendChild(doc.createTextNode(objData.getString("investorName")));
                record.appendChild(fieldInvestorName);

                Element fieldSidNumber = doc.createElement("Field");
                fieldSidNumber.setAttribute("name", "sidNumber");
                fieldSidNumber.appendChild(doc.createTextNode(objData.getString("sidNumber")));
                record.appendChild(fieldSidNumber);

                Element fieldBankAccountNumber = doc.createElement("Field");
                fieldBankAccountNumber.setAttribute("name", "bankAccountNumber");
                fieldBankAccountNumber.appendChild(doc.createTextNode(objData.getString("bankAccountNumber")));
                record.appendChild(fieldBankAccountNumber);

                Element fieldBankCode = doc.createElement("Field");
                fieldBankCode.setAttribute("name", "bankCode");
                fieldBankCode.appendChild(doc.createTextNode(bankCode));
                record.appendChild(fieldBankCode);

                Element fieldActivityDate = doc.createElement("Field");
                fieldActivityDate.setAttribute("name", "activityDate");
                fieldActivityDate.appendChild(doc.createTextNode(new SimpleDateFormat("yyyyMMdd").format(currentDate)));
                record.appendChild(fieldActivityDate);

                Element fieldActivity = doc.createElement("Field");
                fieldActivity.setAttribute("name", "activity");
                fieldActivity.appendChild(doc.createTextNode(objData.getString("activity").substring(0, 1).toUpperCase()));
                record.appendChild(fieldActivity);

                try {
                    Thread.sleep(1);
                } catch (InterruptedException ex) {
                    logger.error("ERROR: " + ex.getMessage());
                }
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            // Uncomment if you do not require XML declaration
            // transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            //A character stream that collects its output in a string buffer,
            //which can then be used to construct a string.
            StringWriter writer = new StringWriter();

            DOMSource source = new DOMSource(doc);
//            StreamResult result = new StreamResult(new File("C:\\file.xml"));   // to write the content into xml file.
//            StreamResult result = new StreamResult(System.out);   //to write the content to your console.
            StreamResult result = new StreamResult(writer); //to write the content to String.

            transformer.transform(source, result);

            stringResult = writer.getBuffer().toString();

            System.out.println("Build request content: " + stringResult);
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }

        returnArray[0] = arrDataRes;
        returnArray[1] = stringResult;
        returnArray[2] = batchReff;

        return returnArray;
    }

    private Object[] buildContentRequestDataValidation(JSONObject jsonRequest) {
        Object[] returnArray = new Object[2];
        JSONArray arrDataRes = new JSONArray();

//        Date currentDate = new Date();
        String stringResult = null;

        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("Message");
            rootElement.setAttribute("name", "dataInvestorValidation");

            doc.appendChild(rootElement);

            // List elements
            Element list = doc.createElement("List");
            list.setAttribute("name", "data");

            rootElement.appendChild(list);

            //loop data here
            JSONArray arrData = jsonRequest.getJSONArray("data");

            for (int i = 0; i < arrData.length(); i++) {
                JSONObject objData = arrData.getJSONObject(i);

                // Record elements
                Element record = doc.createElement("Record");
                record.setAttribute("name", "data");

                list.appendChild(record);

                String extReff = "bjb" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
//                returnArray[0] = extReff;

                //get "externalReference" from request data if exist
                if (jsonRequest.getJSONArray("data").getJSONObject(i).has("extReff")
                        && !jsonRequest.getJSONArray("data").getJSONObject(i).getString("extReff").isEmpty()) {
                    extReff = jsonRequest.getJSONArray("data").getJSONObject(i).getString("extReff");
                }
                
                objData.put("extReff", extReff);
                arrDataRes.put(objData);

                Element fieldExternalReference = doc.createElement("Field");
                fieldExternalReference.setAttribute("name", "externalReference");
                fieldExternalReference.appendChild(doc.createTextNode(extReff));
                record.appendChild(fieldExternalReference);

                if (objData.has("accountNumber") && objData.getString("accountNumber").length() > 4) {
                    Element fieldParticipantId = doc.createElement("Field");
                    fieldParticipantId.setAttribute("name", "participantID");
                    fieldParticipantId.appendChild(doc.createTextNode(objData.getString("accountNumber").substring(0, 5)));
                    record.appendChild(fieldParticipantId);

                    Element fieldAccountNumber = doc.createElement("Field");
                    fieldAccountNumber.setAttribute("name", "accountNumber");
                    fieldAccountNumber.appendChild(doc.createTextNode(objData.getString("accountNumber")));
                    record.appendChild(fieldAccountNumber);
                } else {
                    Element fieldParticipantId = doc.createElement("Field");
                    fieldParticipantId.setAttribute("name", "participantID");
                    fieldParticipantId.appendChild(doc.createTextNode(""));
                    record.appendChild(fieldParticipantId);

                    Element fieldAccountNumber = doc.createElement("Field");
                    fieldAccountNumber.setAttribute("name", "accountNumber");
                    fieldAccountNumber.appendChild(doc.createTextNode(""));
                    record.appendChild(fieldAccountNumber);
                }

                Element fieldSidNumber = doc.createElement("Field");
                fieldSidNumber.setAttribute("name", "sidNumber");
                fieldSidNumber.appendChild(doc.createTextNode(objData.getString("sidNumber")));
                record.appendChild(fieldSidNumber);

                try {
                    Thread.sleep(1);
                } catch (InterruptedException ex) {
                    logger.error("ERROR: " + ex.getMessage());
                }
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            // Uncomment if you do not require XML declaration
            // transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            //A character stream that collects its output in a string buffer,
            //which can then be used to construct a string.
            StringWriter writer = new StringWriter();
//            System.out.println("DOCUMENT: " + getString("externalReference", rootElement));
            DOMSource source = new DOMSource(doc);
//            StreamResult result = new StreamResult(new File("C:\\file.xml"));   // to write the content into xml file.
//            StreamResult result = new StreamResult(System.out);   //to write the content to your console.
            StreamResult result = new StreamResult(writer); //to write the content to String.

            transformer.transform(source, result);

            stringResult = writer.getBuffer().toString();

            System.out.println("Build request content: " + stringResult);
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }

//        return stringResult;
        returnArray[0] = arrDataRes;
        returnArray[1] = stringResult;

        return returnArray;
    }

    protected String getString(String tagName, Element element) {
//        NodeList list = element.getElementsByTagName(tagName);

//        if (list != null && list.getLength() > 0) {
//            NodeList subList = list.item(0).getChildNodes();
//            if (subList != null && subList.getLength() > 0) {
//                return subList.item(0).getNodeValue();
//            }
//        }
//        return null;
        return visitChildNodes(element.getElementsByTagName("Message"), tagName);
    }

    //This function is called recursively
    private static String visitChildNodes(NodeList nList, String tagName) {
        String textContent = null;

        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node node = nList.item(temp);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                System.out.println("Node Name = " + node.getNodeName() + "; Value = " + node.getTextContent());

                //Check all attributes
                if (node.hasAttributes()) {
                    // get attributes names and values
                    NamedNodeMap nodeMap = node.getAttributes();

                    for (int i = 0; i < nodeMap.getLength(); i++) {
                        Node tempNode = nodeMap.item(i);

                        System.out.println("Attr name : " + tempNode.getNodeName() + "; Value = " + tempNode.getNodeValue());

                        if (tempNode.getNodeValue().equals(tagName)) {
                            textContent = node.getTextContent();
                        }
                    }

                    if (node.hasChildNodes()) {
                        //We got more childs; Let's visit them as well
                        textContent = visitChildNodes(node.getChildNodes(), tagName);
                    }
                }
            }
        }

        return textContent;
    }
}
