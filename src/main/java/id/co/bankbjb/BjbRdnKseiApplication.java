package id.co.bankbjb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BjbRdnKseiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BjbRdnKseiApplication.class, args);
	}
}
