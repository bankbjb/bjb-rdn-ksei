/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.bankbjb.rdn.logger.interceptor;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.WebServiceMessage;
import org.springframework.xml.transform.TransformerObjectSupport;

/**
 *
 * @author LENOVO
 */
public class HttpLoggingUtils extends TransformerObjectSupport {
    
    private static final Logger log = LoggerFactory.getLogger(HttpLoggingUtils.class);
    
    private static final String NEW_LINE = System.getProperty("line.separator");

    public HttpLoggingUtils() {
    }
    
    public static void logMessage(String id, WebServiceMessage webServiceMessage) {
        try {
            ByteArrayTransportOutputStream byteArrayTransportOutputStream = new ByteArrayTransportOutputStream();
            webServiceMessage.writeTo(byteArrayTransportOutputStream);
            
            String httpMessage = new String(byteArrayTransportOutputStream.toByteArray());
            log.info(NEW_LINE + "----------------------------" + NEW_LINE + id + NEW_LINE + "----------------------------" + NEW_LINE + httpMessage + NEW_LINE);
        } catch (IOException e) {
            log.error("Unable to log HTTP message.", e);
        }
    }
}
