/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.bankbjb.rdn.logger.interceptor;

import org.springframework.ws.client.WebServiceClientException;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;
import org.springframework.ws.context.MessageContext;

/**
 *
 * @author LENOVO
 */
public class LogHttpHeaderClientInterceptor implements ClientInterceptor {
    
    @Override
    public boolean handleRequest(MessageContext messageContext) throws WebServiceClientException {
        HttpLoggingUtils.logMessage("Client Request Message", messageContext.getRequest());

        return true;
    }

    @Override
    public boolean handleResponse(MessageContext messageContext) throws WebServiceClientException {
        HttpLoggingUtils.logMessage("Client Response Message", messageContext.getResponse());

        return true;
    }

    @Override
    public boolean handleFault(MessageContext arg0) throws WebServiceClientException {
        // No-op
        return true;
    }

    @Override
    public void afterCompletion(MessageContext arg0, Exception arg1) throws WebServiceClientException {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        // No-op
    }
}
