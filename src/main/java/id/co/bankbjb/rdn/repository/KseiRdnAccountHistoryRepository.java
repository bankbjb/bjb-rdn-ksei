/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.bankbjb.rdn.repository;

import id.co.bankbjb.rdn.model.KseiRdnAccountHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author LENOVO
 */
@Repository
public interface KseiRdnAccountHistoryRepository extends JpaRepository<KseiRdnAccountHistory, String> {
    
}
